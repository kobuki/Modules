<?php
/*
Copyright (c) 2017, Florian Schaal, schaal @it
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ISPConfig nor the names of its contributors
      may be used to endorse or promote products derived from this software without
      specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

class dns_dns_slave_auto_plugin {

	var $plugin_name = 'dns_dns_slave_auto_plugin';
	var $class_name = 'dns_dns_slave_auto_plugin';
	var $nameservers = array();
	var $dns_slave_server_id = array();

	public function __construct() {
		//* define your dns-ip(s) that the slave-server should accept the data from (same as NS (IP-address) in Secondary DNS Zones)
		$this->nameservers[] = '192.168.0.1';
		$this->nameservers[] = 'fe80::2';
		//* define the server_ids for slave-server(s) (see server_id in dbispconfig.server on the master)
		$this->dns_slave_server_id[] = 2;
	}

	function onLoad() {
		global $app;

		$app->plugin->registerEvent('dns:dns_soa:on_after_insert', 'dns_dns_slave_auto_plugin', 'slave_insert');
		$app->plugin->registerEvent('dns:dns_soa:on_after_update', 'dns_dns_slave_auto_plugin', 'slave_update');
		$app->plugin->registerEvent('dns:dns_soa:on_after_delete', 'dns_dns_slave_auto_plugin', 'slave_delete');
		$app->plugin->registerEvent('dns:wizard:on_after_insert',  'dns_dns_slave_auto_plugin', 'slave_insert');
	}

	function slave_insert($event_name, $page_form) {
		global $app;

		$soa = $app->db->queryOneRecord("SELECT * FROM dns_soa WHERE id = ?", $page_form->id);
		$insert_data = array(
			'sys_userid' => $soa['sys_userid'],
			'sys_groupid' => $soa['sys_groupid'],
			'sys_perm_user' => $soa['sys_perm_user'],
			'sys_perm_group' => $soa['sys_perm_group'],
			'sys_perm_other' => $soa['sys_perm_other'],
			'origin' => $soa['origin'],
			'ns' => implode(',', $this->nameservers),
			'active' => $soa['active'],
			'xfer' => ''
		);
		foreach($this->dns_slave_server_id as $slave) {
			$insert_data['server_id'] = $slave;
			$app->db->datalogInsert('dns_slave', $insert_data, 'id');
		}
	}

	function slave_update($event_name, $page_form) {
		global $app;

		if($page_form->dataRecord['origin'] != $page_form->oldDataRecord['origin']) {
			$slave_zone_recs = $app->db->queryAllRecords("SELECT * FROM dns_slave WHERE origin = ?", $page_form->oldDataRecord['origin']);
			if(!$slave_zone_recs) $this->slave_insert($event_name, $page_form);
			foreach($slave_zone_recs as $slave_zone) {
				$app->db->datalogDelete('dns_slave', 'id', $slave_zone['id']);
				$this->slave_insert($event_name, $page_form);
			}
		}
		if($page_form->dataRecord['active'] != $page_form->oldDataRecord['active']) {
			$slave_zone_recs = $app->db->queryAllRecords("SELECT * FROM dns_slave WHERE origin = ?", $page_form->dataRecord['origin']);
			if(!$slave_zone_recs) $this->slave_insert('update_insert', $page_form);
			foreach($slave_zone_recs as $slave_zone) {
				if($page_form->dataRecord['active'] == 'Y') {
					$app->db->datalogUpdate('dns_slave', array('active' => 'Y'), 'id', $slave_zone['id']);
				} else {
					$app->db->datalogUpdate('dns_slave', array('active' => 'N'), 'id', $slave_zone['id']);
				}
			}
		}
	}

	function slave_delete($event_name, $page_form) {
		global $app;

		$slave_zone_recs = $app->db->queryAllRecords("SELECT * FROM dns_slave WHERE origin = ?", $page_form->dataRecord['origin']);
		foreach($slave_zone_recs as $slave_zone) {
			$app->db->datalogDelete('dns_slave', 'id', $slave_zone['id']);
		}
	}

}
